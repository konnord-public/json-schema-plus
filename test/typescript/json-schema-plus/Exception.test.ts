import { expect } from "chai";
import { LoggedError } from "../../../src/typescript/json-schema-plus/Common";

class MockException extends LoggedError {}

describe( "Exception", () => {

    it( "instanceof must be work on the exception catch", () => {

        try{
            throw new MockException( "mock message" );
        } catch( error ) {
            expect( error ).to.be.instanceof( Error ).
            and.to.be.instanceof( LoggedError ).
            and.to.be.instanceof( MockException );
        }
    });

    it( "chai accept the Exception as an Error object", () => {

        function test(){
            console.log( new MockException("TEST") );
            throw new MockException( "mock message" );
        }

        expect( test ).
        and.that.throw( LoggedError ).
        and.instanceof( MockException ).
        and.include({
            message: "mock message"
        });

    });

    it( "Any kind of error can become an logged error object", () => {

        let error : Error = undefined;

        expect( () => { throw LoggedError.of( error = new Error( 'Not logged error' ) ); } ).
        and.that.throw( LoggedError ).
        and.include({
            message: `From ${ error.stack }`
        });

        let message = 'A message error not logged';

        expect( () => { throw LoggedError.of( message ); } ).
        and.that.throw( LoggedError ).
        and.include({
            message: `From ${ message }`
        });

    });

    it( "An logged error can't be logged twice", () => {

        let error = new LoggedError( 'An error message' );
        
        expect( error === LoggedError.of( error ) ).to.be.true;
    });
});