# -- Grunt dependancie ----------------------------------------------------------------------------
fs = require "fs"
glob = require "glob"

# -- Grunt variable -------------------------------------------------------------------------------
configuration = {}
global_tsconfig = JSON.parse fs.readFileSync "tsconfig.json"

build = {
    srcFolder: 'src',
    testFolder: 'test'
    output: "#{ __dirname }/target"
}

typescript = {
    srcFolder: "#{ build.srcFolder }/typescript",
    testFolder: "#{ build.testFolder }/typescript"
    output: "#{ build.output }/typescript",
    mochaDependancies: [ "ts-node/register", "source-map-support/register" ]
}

mocha = {
    output: "#{ build.output }/mocha"
}

nyc = {
    output: "#{ build.output }/nyc"
    reporter: ['lcov', 'text-summary', 'html'],
    ifErrorOnComplete: (grunt) -> (err, stdout, callback) ->
        if err then grunt.fail.fatal( "An error has occur during the Mocha test.", err.code )
        callback()
}

defaultTasks = []

# -- Generic function -----------------------------------------------------------------------------
removeLastSlashOfFolderPath = ( path ) -> path.substring( 0, path.length - 1 )

# -- Grunt configure ------------------------------------------------------------------------------
configureTypescriptProjectGruntTask = ( grunt ) ->

    glob.sync( "*/" , {
        cwd: typescript.srcFolder
    }).map( removeLastSlashOfFolderPath ).
    forEach( (folder) -> configureTypescriptProgramGruntTask( grunt, folder ) )

configureTypescriptProgramGruntTask = ( grunt, programFolder  ) ->

    # Program variable
    program = {
        srcFolder: "#{ typescript.srcFolder }/#{ programFolder }",
        testFolder: "#{ typescript.testFolder }/#{ programFolder }",
        typescriptOutput: "#{ typescript.output }/#{ programFolder }",
        mochaOuput: "#{ mocha.output }/#{ programFolder }",
        nycOuput: "#{ nyc.output }/#{ programFolder }"
    }

    # Typescript

    ## Create an storage folder for all the content created for this program
    grunt.file.mkdir program.typescriptOutput

    ## Load local typescript compilation rule overided of global
    local_tsconfig = undefined
    local_tsconfigPath = "#{ program.srcFolder }/tsconfig.json"
    if fs.existsSync local_tsconfigPath
    then do ->
        tsconfig = JSON.parse fs.readFileSync local_tsconfigPath
        local_tsconfig = Object.assign {}, global_tsconfig, tsconfig
    else local_tsconfig = Object.assign {}, global_tsconfig
    local_tsconfig.compilerOptions.rootDir = program.srcFolder

    ## create ts task on typescript src file
    configuration.ts[ "#{ programFolder }" ] = {
        files: {
            "#{ program.typescriptOutput }": glob.sync( "**/*.ts" , {
                cwd: program.srcFolder
            }).map( (pathToAFile) -> "#{ __dirname }/#{ program.srcFolder }/#{ pathToAFile }" )
        },
        options: local_tsconfig.compilerOptions
    }

    # Mocha

    ## Create an storage folder for the log from mocha
    grunt.file.mkdir program.mochaOuput

    ## create mocha test task to test the program

    ###
    In the test program folder to parralize some group them
    into a folder the first folder become a group
    ###
    mochaGroups = glob.sync( "*/" , {
        cwd: program.testFolder
    }).map( removeLastSlashOfFolderPath )

    mochaGroups.forEach( (group) ->
        configuration.mochaTest[ "#{ programFolder }@#{ group }" ] = {
            options: {
                reporter: 'spec',
                captureFile: "#{ program.mochaOuput }/#{ group }.log"
                require: typescript.mochaDependancies
            }
            src: [ "#{ __dirname }/#{ program.testFolder }/#{ group }/**/*.test.ts" ]
        }
    )
        

    ###
    Any other file create at the root of the test folder
    are stored in the group default
    ###
    configuration.mochaTest[ "#{ programFolder }@default" ] = {
        options: {
            reporter: 'spec',
            captureFile: "#{ program.mochaOuput }/default.log"
            require: typescript.mochaDependancies
        }
        src: [ "#{ __dirname }/#{ program.testFolder }/*.test.ts" ]
    }

    # nyc

    ###
    Each typescript program have a coverage task for each test task.
    ###
    Object.keys( configuration.mochaTest ).
    forEach( (testTask) ->
        configuration.nyc[ testTask ] = {
            options: {
                cwd: ".",
                include: [ "#{ program.srcFolder }/**" ],
                extension: [
                    ".ts",
                    ".tsx"
                ],
                reporter: nyc.reporter,
                reportDir: "#{ program.nycOuput }/#{ testTask }",
                all: true
            },
            cmd: false,
            args: ['grunt', "mochaTest:#{ testTask }" ],
            onComplete: nyc.ifErrorOnComplete grunt
        }
    )

    # Register a custom task to build this program
    programTaskName = "build-typescript@#{ programFolder }"
    programTasks = [].concat(
        mochaGroups.map( (group) -> "mochaTest:#{ programFolder }@#{ group }" ),
        [
            "mochaTest:#{ programFolder }@default",
            "ts:#{ programFolder }"
        ]
    )

    grunt.registerTask programTaskName, programTasks
    defaultTasks.push programTaskName


# -- Grunt init -----------------------------------------------------------------------------------
module.exports = ( grunt ) ->

## - Grunt initialize confiugration to an default value -------------------------------------------

    configuration.ts = {}
    configuration.mochaTest = {}
    configuration.nyc = {}

## - Grunt configure by program task --------------------------------------------------------------
    
    configureTypescriptProjectGruntTask grunt
    
    ###
    Configure nyc to merge all report generated into one.
    ###
    configuration.nyc.report = {
        options: {
            reporter: nyc.reporter,
            reportDir: nyc.output
        }
    }

    grunt.initConfig configuration

## - Grunt load plugin ----------------------------------------------------------------------------

    grunt.loadNpmTasks "grunt-ts"
    grunt.loadNpmTasks "grunt-mocha-test"
    grunt.loadNpmTasks "grunt-simple-nyc"

## - Grunt register tasks --------------------------------------------------------------------------

    grunt.registerTask "default", defaultTasks