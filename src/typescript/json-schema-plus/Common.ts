import * as log4js from "log4js";

/**
 * All instance of this error are logged on
 * the console even they are catch.
 */
export class LoggedError extends Error {

    /**
     * Create a new logged error
     * @param {string} message -  
     */
    constructor( message : string ){
        super( message );
        this.name = this.constructor.name;
        logger.error( this.stack );
    }

    /**
     * Cast any kind of object in logged RestApiException
     * @param error is
     */
    static of( error : any ) : LoggedError{

        if( error instanceof LoggedError )
            return error;

        if( error instanceof Error )
            return new LoggedError( `From ${ error.stack }` );

        return new LoggedError( `From ${ error }` );
    }
}


/**
 * Environement who must be contains inside the environement variable
 */
interface Environement {

    /**
     * The level of log must be display on execution
     */
    JSON_SCHEMA_PLUS_LOG_LEVEL : string
}

/**
 * In an environement all the variable are only string
 */
type RawEnvironemnt = { readonly [ key in keyof Environement ]: string }

/**
 * Default value if the environement variable is missing
 */
const defaultEnv : RawEnvironemnt = {
    JSON_SCHEMA_PLUS_LOG_LEVEL: "info"
}

/**
 * Make the environment as the right type
 * 
 * @param {RawEnvironemnt} rawEnv to cast as an Environement object
 * 
 * @return an environement object
 */
export function castEnv( rawEnv : RawEnvironemnt ) : Environement {
    return {
        JSON_SCHEMA_PLUS_LOG_LEVEL: rawEnv.JSON_SCHEMA_PLUS_LOG_LEVEL
    };
}

/**
 * The environement usable at the runtime
 */
export const env = castEnv(
    Object.assign( {}, defaultEnv, process.env )
);

/**
 * The logger for the out of the program
 */
export const logger = log4js.getLogger( "json-schema-plus" );
logger.level = env.JSON_SCHEMA_PLUS_LOG_LEVEL;